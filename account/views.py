from django import shortcuts
# Create your views here.
from django.contrib.auth import login
from django.urls import reverse
from django.views import View

from account import models


class OAuthLogin(View):
    http_method_names = ['get']

    # noinspection PyMethodMayBeStatic
    def get(self, request, provider):
        service = shortcuts.get_object_or_404(models.OAuthService, name=provider)
        request.session['next'] = request.GET.get('next', '/admin')
        return shortcuts.redirect(
            service.provider.get_authorization_url(
                redirect_uri=request.build_absolute_uri(
                    reverse('oauth_callback', kwargs={"provider": provider})
                )
            ))


class OAuthCallback(View):
    http_method_names = ['get']

    # noinspection PyMethodMayBeStatic
    def get(self, request, provider):
        oauth_service = shortcuts.get_object_or_404(models.OAuthService, name=provider)
        service = oauth_service.provider
        code = request.GET.get('code', None)
        if code is None:
            return shortcuts.redirect(request.session['next'])
        uri = request.build_absolute_uri(reverse('oauth_callback', kwargs={"provider": provider}))
        token = service.retrieve_token(code, uri)
        user = service.login_with_token(token, oauth_service)
        if user is not None:
            login(request, user)
        return shortcuts.redirect(request.session['next'])
