= Raid GPS
Gabriel Augendre <gabriel@augendre.info>

This project intends to provide an easy-to-use interface for drivers at Raid INSA Lyon Orange.

== Dependencies
This project uses `pipenv` to manage the dependencies. You can install `pipenv` with `pip` :
----
pip install pipenv
----

Then install the dependencies :
----
pipenv install
----

You'll also need to generate a service worker file. For this, you'll need `workbox-cli` from npm.
----
npm install -g workbox-cli
workbox generateSW
----

=== Service Worker
The service worker allows offline use of the app if resource have been cached by a first visit.

== Development
=== Run server
Run the server with the following :
----
pipenv run ./manage.py runserver 8000
----

=== Initial setup
You need a year and days before using the app. A migration takes care of generating them for you.

== License
Raid GPS is licensed under the GNU GPL v3.

Copyright (C) 2018 Gabriel Augendre +
Copyright (C) 2018 BdE INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

