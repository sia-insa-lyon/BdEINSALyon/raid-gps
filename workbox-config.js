module.exports = {
    swDest: "gps/templates/gps/sw.js",
    globDirectory: '.',
    globPatterns: [],

    // Define runtime caching rules.
    runtimeCaching: [
        {
            urlPattern: /\//,
            handler: 'NetworkFirst',

            options: {
                // Fall back to the cache after 10 seconds.
                networkTimeoutSeconds: 10,
            },
        },
    ],
};
