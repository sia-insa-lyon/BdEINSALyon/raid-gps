from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone

from gps.fields import CoordinateField


class BaseModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class YearManager(models.Manager):
    def latest(self):
        return Year.objects.order_by('end').last()


class Year(BaseModel):
    """
    Représente une édition du Raid. Les dates de début/fin sont utilisées pour connaître l'édition la plus récente.
    """

    class Meta:
        verbose_name = 'édition'
        verbose_name_plural = 'éditions'
        ordering = ('-end',)

    name = models.CharField('nom', max_length=100)
    start = models.DateTimeField('début', default=timezone.now)
    end = models.DateTimeField('fin', default=timezone.now)

    objects = YearManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('year-detail', kwargs={'pk': self.pk})


class Day(BaseModel):
    """
    Représente une journée du Raid. N'est pas relié à une :model:`gps.Year` en particulier.
    """

    class Meta:
        verbose_name = 'jour'
        verbose_name_plural = 'jours'

    name = models.CharField('nom', max_length=20, help_text='J1, J2, ...')

    def __str__(self):
        return self.name


class NavigationProvider(BaseModel):
    """
    Un fournisseur de navigation est un service externe qui peut fournir des instructions de navigation vers un lieu
    spécifique, connaissant ses coordonnées GPS.

    Le schéma URL doit include `{latitude}` et `{longitude}` où cela est nécessaire.
    """

    class Meta:
        verbose_name = 'fournisseur de navigation'
        verbose_name_plural = 'fournisseurs de navigation'

    APP = 'M'  # Ex "mobile" -> M
    WEB = 'D'  # Ex "desktop" -> D
    BOTH = 'B'

    _TYPE_CHOICES = (
        (APP, 'App'),
        (WEB, 'Web'),
        (BOTH, 'App & Web'),
    )

    name = models.CharField('nom', max_length=100)
    url_scheme = models.CharField('schéma URL', max_length=500,
                                  help_text="Le schéma doit inclure <code>{latitude}</code> et "
                                            "<code>{longitude}</code> où cela est nécessaire.")
    type = models.CharField(max_length=1, choices=_TYPE_CHOICES,
                            help_text="Le type est utilisé pour le tri et l'affichage uniquement.")

    @property
    def icon(self):
        if self.type == self.APP:
            return 'mobile'
        elif self.type == self.WEB:
            return 'desktop'
        else:
            return 'link'

    def __str__(self):
        return f'{self.name} ({self.get_type_display()})'


class Place(BaseModel):
    """
    Représente un lieu du Raid. Un lieu est défini par ses coordonnées GPS décimales et un nom.

    Il est relié à une :model:`gps.Year` et plusieurs :model:`gps.Day`.
    """
    class Meta:
        verbose_name = 'lieu'
        verbose_name_plural = 'lieux'

    name = models.CharField('nom', max_length=300)
    days = models.ManyToManyField(
        to=Day,
        verbose_name='jours',
        related_name='places'
    )
    year = models.ForeignKey(
        to=Year,
        verbose_name='édition',
        on_delete=models.PROTECT,
        related_name='places'
    )
    latitude = CoordinateField('latitude')
    longitude = CoordinateField('longitude')

    def get_absolute_url(self):
        return reverse('day-detail', kwargs={'pk': self.days.first().pk, 'year_pk': self.year.pk})

    def str_days(self):
        return ', '.join(map(str, self.days.all()))

    def __str__(self):
        return self.name

    @property
    def latitude_str(self):
        return str(self.latitude)

    @property
    def longitude_str(self):
        return str(self.longitude)

    def navigation_providers(self):
        lst = []
        for provider in NavigationProvider.objects.order_by('-type', 'name'):
            lst.append({
                'provider': provider,
                'url': provider.url_scheme.format(latitude=self.latitude, longitude=self.longitude)
            })
        return lst
