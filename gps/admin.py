from django.contrib import admin

from gps.models import Year, Place, Day, NavigationProvider


@admin.register(Year)
class YearAdmin(admin.ModelAdmin):
    list_display = ['name', 'start', 'end']
    date_hierarchy = 'start'


@admin.register(Day)
class DayAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ['name', 'str_days', 'year', 'latitude', 'longitude']
    list_filter = ['days', 'year']


@admin.register(NavigationProvider)
class NavigationProviderAdmin(admin.ModelAdmin):
    list_display = ['name', 'url_scheme', 'type']
