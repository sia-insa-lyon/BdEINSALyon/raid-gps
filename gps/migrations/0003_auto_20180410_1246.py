# Generated by Django 2.0.4 on 2018-04-10 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gps', '0002_auto_20180410_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='place',
            name='latitude',
            field=models.DecimalField(decimal_places=8, max_digits=11, verbose_name='latitude'),
        ),
        migrations.AlterField(
            model_name='place',
            name='longitude',
            field=models.DecimalField(decimal_places=8, max_digits=11, verbose_name='longitude'),
        ),
    ]
