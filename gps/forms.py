from django import forms

from gps.models import Year


class ImportPlacesForm(forms.Form):
    year = forms.ModelChoiceField(Year.objects.all())
    file = forms.FileField()
