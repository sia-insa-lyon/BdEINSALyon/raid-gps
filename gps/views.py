from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db import transaction
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views import generic

from gps.forms import ImportPlacesForm
from gps.mixins import QuickActionsMixin
from gps.models import Year, Day, Place


class LatestYear(LoginRequiredMixin, generic.View):
    """
    Redirige vers la dernière édition ou affiche une page d'erreur s'il n'y a pas d'édition.

    Template
    --------
    :template:`gps/no-year.html`
    """
    def get(self, request, *args, **kwargs):
        year = Year.objects.latest()
        if year:
            return redirect('year-detail', pk=year.pk)
        return render(request, 'gps/no-year.html')


class YearsListView(LoginRequiredMixin, generic.ListView):
    """
    Liste toutes les éditions.

    Template
    --------
    :template:`gps/years-list.html`
    """
    model = Year
    template_name = 'gps/years-list.html'
    context_object_name = 'years'


class YearDetailView(LoginRequiredMixin, QuickActionsMixin, generic.DetailView):
    """
    Affiche une édition avec les jours et tous les lieux associés.

    Context
    -------
    ``days``
        Liste de tous les :model:`gps.Day`.
    ``places``
        Liste de tous les :model:`gps.Place` associés à cette édition.

    Template
    --------
    :template:`gps/year-detail.html`
    """
    model = Year
    template_name = 'gps/year-detail.html'
    context_object_name = 'year'

    def get_quick_actions(self):
        return [
            {
                'url': reverse('years-list'),
                'category': 'secondary',
                'display': 'Toutes les éditions'
            },
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['days'] = Day.objects.all()
        context['places'] = Place.objects.filter(year=self.object)

        return context


class DayDetailView(LoginRequiredMixin, QuickActionsMixin, generic.DetailView):
    """
    Affiche tous les lieux associés à un jour et une édition particuliers.

    Le jour et l'édition doivent exister.

    Context
    -------
    ``places``
        Liste de tous les :model:`gps.Place` associés à l'édition et au jour spécifiés.

    Template
    --------
    :template:`gps/day-detail.html`
    """
    model = Day
    template_name = 'gps/day-detail.html'
    context_object_name = 'day'
    year = None

    def dispatch(self, request, *args, year_pk=None, **kwargs):
        self.year = get_object_or_404(Year, pk=year_pk)
        return super().dispatch(request, *args, **kwargs)

    def get_quick_actions(self):
        return [
            {
                'url': reverse('year-detail', kwargs={'pk': self.year.pk}),
                'category': 'secondary',
                'display': f'Retour à {self.year}'
            },
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['places'] = Place.objects.filter(year=self.year, days=self.object)
        context['year'] = self.year

        return context


class PlaceMapView(LoginRequiredMixin, QuickActionsMixin, generic.DetailView):
    """
    Affiche la carte d'un lieu.

    Template
    --------
    :template:`gps/place-map.html`
    """
    model = Place
    template_name = 'gps/place-map.html'
    context_object_name = 'place'
    day = None

    def dispatch(self, request, *args, **kwargs):
        day_pk = request.GET.get('day')
        if day_pk:
            self.day = get_object_or_404(Day, pk=day_pk)

        return super().dispatch(request, *args, **kwargs)

    def get_quick_actions(self):
        if self.day:
            return [
                {
                    'url': reverse('day-detail', kwargs={'year_pk': self.object.year.pk, 'pk': self.day.pk}),
                    'category': 'secondary',
                    'display': f'Retour à {self.object.year} - {self.day}'
                },
            ]
        else:
            return [
                {
                    'url': reverse('year-detail', kwargs={'pk': self.object.year.pk}),
                    'category': 'secondary',
                    'display': f'Retour à {self.object.year}'
                },
            ]


class ImportPlacesView(LoginRequiredMixin, PermissionRequiredMixin, generic.FormView):
    """
    Permet l'importation de lieux depuis un fichier Excel.

    Accessible uniquement aux utilisateurs avec la permission "add_place".

    Le fichier doit contenir au minimum les colonnes suivantes :

    * nom
    * jours
    * latitude
    * longitude

    Les autres colonnes seront ignorées.

    Template
    --------
    :template:`gps/import-data.html`
    """
    form_class = ImportPlacesForm
    template_name = 'gps/import-data.html'
    year = None

    permission_required = 'gps.add_place'
    raise_exception = True

    def get_success_url(self):
        return reverse('year-detail', kwargs={'pk': self.year.pk})

    def get_initial(self):
        return {'year': Year.objects.latest()}

    def form_valid(self, form):
        self.year = form.cleaned_data['year']
        file = form.files['file'].file

        from openpyxl import load_workbook
        wb = load_workbook(filename=file)
        ws = wb.active

        iterator = iter(ws)
        first_line = next(iterator)
        keys = {}
        try:
            for index, cell in enumerate(first_line):
                key = cell.value.lower()
                if key in ('nom', 'name'):
                    keys['name'] = index
                elif key in ('day', 'jour', 'days', 'jours'):
                    keys['days'] = index
                elif key == 'latitude':
                    keys['latitude'] = index
                elif key == 'longitude':
                    keys['longitude'] = index

            assert len(keys) == 4

        except (AttributeError, AssertionError):
            messages.error(
                self.request,
                '<strong>Contenu non reconnu.</strong> '
                'Merci de vérifier que les colonnes suivantes '
                'sont présentes : "nom", "jours", "latitude" et "longitude".'
            )
            return super().form_invalid(form)

        days_pk = {day.name.lower(): day.pk for day in Day.objects.all()}

        counter = 0

        # We wrap the whole block inside a transaction to ensure all the places are created or none.
        # The KeyError might happen when a day specified in the Excel file doesn't match any of the
        # days in the database.
        try:
            with transaction.atomic():
                for line in iterator:
                    mapping = {'year': self.year}
                    days = []
                    for key, index in keys.items():
                        value = line[index].value
                        if key == 'days':
                            value = value.lower().split(',')
                            for day in value:
                                days.append(days_pk[day])
                        else:
                            mapping[key] = value
                    place = Place.objects.create(**mapping)
                    place.days.add(*days)
                    counter += 1
        except KeyError:
            messages.error(
                self.request,
                '<strong>Contenu non reconnu</strong> '
                'Merci de vérifier que les jours spécifiés dans le fichier '
                'ont été créés dans la base de données avant l\'import.'
            )
            return super().form_invalid(form)
        else:
            messages.success(self.request, f'Ajout de {counter} lieu(x) avec succès.')

        return super().form_valid(form)
