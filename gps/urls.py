from django.urls import path

from gps import views

urlpatterns = [
    path('', views.LatestYear.as_view(), name='latest-year'),
    path('years-list', views.YearsListView.as_view(), name='years-list'),
    path('year/<int:pk>', views.YearDetailView.as_view(), name='year-detail'),
    path('year/<int:year_pk>/day/<int:pk>', views.DayDetailView.as_view(), name='day-detail'),
    path('place/<int:pk>/map', views.PlaceMapView.as_view(), name='place-map'),
    path('import', views.ImportPlacesView.as_view(), name='import-data'),
]
