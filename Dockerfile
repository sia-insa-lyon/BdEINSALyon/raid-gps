FROM node:10 as node

RUN npm install -g workbox-cli

WORKDIR /app

COPY workbox-config.js ./
RUN mkdir -p gps/templates/gps/
RUN workbox generateSW


FROM python

RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

WORKDIR /app
EXPOSE 8000
VOLUME /app/staticfiles

ENV DATABASE_URL postgres://postgresql:postgresql@db:5432/raid_gps
ENV SECRET_KEY ''
ENV MAILGUN_ACCESS_KEY ''
ENV MAILGUN_SERVER_NAME ''
ENV DJANGO_ENV ''
ENV ADMIN_EMAIL ''
ENV SERVER_EMAIL ''
ENV SITE_URL ''

RUN pip3 install pipenv
COPY Pipfile ./
RUN pipenv install

COPY . ./
COPY --from=node /app/gps/templates/gps/sw.js gps/templates/gps/sw.js

RUN chmod +x bash/run-prod.sh
CMD bash/run-prod.sh
